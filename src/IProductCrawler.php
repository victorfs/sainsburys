<?php
namespace Victorfs\Sainsburys;

interface IProductCrawler
{
	public function getTitleAndUnitPrice($htmlData);
	public function getSizeAndDescription($htmlData);
}