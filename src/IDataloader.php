<?php
namespace Victorfs\Sainsburys;

interface IDataloader
{
	public function getUri();
	public function setUri($uri);
	public function getQueryParameters();
	public function setQueryParameters($queryParameters);
	public function getData();
}