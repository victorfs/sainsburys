<?php
namespace Victorfs\Sainsburys;

/**
* SainsburysApp is the main class
*
* @package  Sainsburys
* @author   Victor Fernandez <victorfs.uk@gmail.com>
*/
class SainsburysApp
{
	
	private $_dataLoader;
	private $_productCrawler;
	private $_allProducts;

	/**
	* Create a new App.
	*
	* @param  IDataloader $dataLoader
	* @param  IProductCrawler $productCrawler
	*/
	public function __construct( IDataloader $dataLoader, IProductCrawler $productCrawler)
	{
		$this->_dataLoader = $dataLoader; 
		$this->_productCrawler = $productCrawler;
		$this->buildAllProducts();
	}

    /**
    * Build an array of all the products
    */
	private function buildAllProducts()
	{
		try
		{
			$htmlData = $this->_dataLoader->getData();
			$products = $this->_productCrawler->getTitleAndUnitPrice($htmlData);
			$allProducts = array();
			$totalPrice = 0;
			foreach ($products as $product)
			{
				$this->_dataLoader->setUri($product['product_info_link']);
				$htmlData = $this->_dataLoader->getData();
				$productInfo = $this->_productCrawler->getSizeAndDescription($htmlData);
				$allProducts['results'][] = array(
					'title' => $product['title'],
					'size' => round($productInfo['size'],1).'kb',
					'unit_price' => round($product['unit_price'],2),
					'description' => $productInfo['description']
					);
				$totalPrice += floatval($product['unit_price']);
			}
			$allProducts['total'] = round($totalPrice,2);
			$this->_allProducts = $allProducts;
		}
		catch( Exception $e )
		{
			$this->_allProducts = array();
		}
	}

    /**
    * Return an array of all the products
    *
    * @return array
    */
	public function getAllProdutcs()
	{
		return $this->_allProducts;
	}

    /**
    * Return a JSON array of all the products
    *
    * @return JSON
    */
	public function getAllProdutcsJSON()
	{
		return json_encode($this->_allProducts);
	}
}