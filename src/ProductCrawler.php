<?php
namespace Victorfs\Sainsburys;

use \Symfony\Component\DomCrawler\Crawler;

/**
* ProductCrawler has to crawl a specific html to find Title, Unit price, Size and Description
*
* @package  Sainsburys
* @author   Victor Fernandez <victorfs.uk@gmail.com>
*/
class ProductCrawler implements IProductCrawler
{
	/**
	* Return Title, Unit price and product info link
	*
	* @param stream $htmlData
	* @return array
	*/
	public function getTitleAndUnitPrice($htmlData)
	{
		try
		{
			$crawler = new Crawler();
			$crawler->addHtmlContent($htmlData, 'UTF-8');
			$filter = $crawler->filter('.product');
			$products = array();
			foreach ($filter as $productSection)
			{
				$crawler = new Crawler($productSection);
				$title = trim($crawler->filter('h3')->text());
				$pricePerUnit = trim($crawler->filter('.pricePerUnit')->text());
				$pricePerUnit = str_replace("&pound", "" , str_replace("/unit", "" , $pricePerUnit));
				$productInfoLink = $crawler->filter('.productInfo a')->attr('href');
				$products[] = array('title' => $title, 'unit_price' => $pricePerUnit, 'product_info_link' => $productInfoLink);
			}
		}
		catch(\Exception $e)
		{
			throw $e;
		}
		return $products;
	}

	/**
	* Return file size and description of the a specific product
	*
	* @param stream $htmlData
	* @return array
	*/
	public function getSizeAndDescription($htmlData)
	{
		try
		{
			$crawler = new Crawler();
			$crawler->addHtmlContent($htmlData, 'UTF-8');
			$size = strlen($htmlData)/1024;
			$description = trim($crawler->filter('#information .productText')->first()->text());
			$productInfo = array('size' => $size, 'description' => $description);
		}
		catch(\Exception $e)
		{
			throw $e;
		}
		return $productInfo;
	}
}