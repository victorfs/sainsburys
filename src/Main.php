<?php
namespace Victorfs\Sainsburys;

require 'vendor/autoload.php';

$uri = 'http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html';

$dataloader = new Dataloader($uri);
$productCrawler = new ProductCrawler();

$app = new SainsburysApp($dataloader, $productCrawler);
echo $app->getAllProdutcsJSON();
