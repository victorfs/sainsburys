<?php
namespace Victorfs\Sainsburys;

use GuzzleHttp\Client;

/**
* Dataloader has to manage http request to retrieve the data
*
* @package  Sainsburys
* @author   Victor Fernandez <victorfs.uk@gmail.com>
*/
class Dataloader implements IDataloader 
{
	private $_uri;
	private $_queryParameters;

	/**
	* Create a new Dataloader.
	*
	* @param  string $uri
	* @param  array $queryParameters array('var1'=> value1, 'var2' => value2, ...)
	*/
	function __construct($uri, $queryParameters = array())
	{
		$this->_uri = $uri;
		$this->_queryParameters = $queryParameters;
	}

	/**
	* Return uri
	*
	* @return string
	*/
	public function getUri()
	{
		return $this->_uri;
	}

	/**
	* Set uri
	*
	* @param string $uri
	*/
	public function setUri($uri)
	{
		$this->_uri = $uri;
	}

	/**
	* Return the query parameters
	*
	* @return array
	*/
	public function getQueryParameters()
	{
		return $this->_queryParameters;
	}

	/**
	* Set queryParameters array('var1'=> value1, 'var2' => value2, ...)
	*
	* @param array $queryParameters
	*/
	public function setQueryParameters($queryParameters)
	{
		$this->_queryParameters = $queryParameters;
	}

	/**
	* Return the data requested
	*
	* @return stream
	*/
	public function getData()
	{
		try
		{
			$client = new Client();
			$response = $client->request('GET', $this->_uri, array('query' => $this->_queryParameters));
		}
		catch(\Exception $e)
		{
			throw $e;
		}
		return $response->getBody();
	}
}