## Synopsis

Console application that scrapes the Sainsbury’s grocery site - Ripe Fruits page and returns a JSON array of all the products on the page.

## Requirements

PHP 5.5+
Composer 1+

## Installation

composer install

## Usage

php ./src/Main.php 

## Tests

./vendor/bin/phpunit