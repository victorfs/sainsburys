<?php
namespace Victorfs\Sainsburys\Tests;

use Victorfs\Sainsburys\ProductCrawler;
use Victorfs\Sainsburys\Dataloader;

class ProductCrawlerTest extends \PHPUnit_Framework_TestCase
{

	private $_dataLoader;
	private $_productCrawler;

	public function setUp()
	{
		$uri = 'http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html';
		$this->_dataLoader = new Dataloader($uri);
		$this->_productCrawler = new ProductCrawler();
	}

	public function testTitleAndUnitPrice()
	{
		$htmlData = $this->_dataLoader->getData();
		$products = $this->_productCrawler->getTitleAndUnitPrice($htmlData);
		$this->assertInternalType('array', $products);
		foreach ($products as $product)
		{
			$this->assertArrayHasKey('title', $product);
			$this->assertArrayHasKey('unit_price', $product);
			$this->assertArrayHasKey('product_info_link', $product);
		}
	}

	public function testSizeAndDescription()
	{
		$htmlData = $this->_dataLoader->getData();
		$products = $this->_productCrawler->getTitleAndUnitPrice($htmlData);
		foreach ($products as $product)
		{
			$this->_dataLoader->setUri($product['product_info_link']);
			$htmlData = $this->_dataLoader->getData();
			$productInfo = $this->_productCrawler->getSizeAndDescription($htmlData);
			$this->assertInternalType('array', $productInfo);
			$this->assertArrayHasKey('size', $productInfo);
			$this->assertArrayHasKey('description', $productInfo);
		}
	}

}