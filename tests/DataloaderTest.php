<?php
namespace Victorfs\Sainsburys\Tests;

use Victorfs\Sainsburys\Dataloader;

class DataloaderTest extends \PHPUnit_Framework_TestCase
{
	public function testUri()
	{
		$uri = 'http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html';
		$dataLoader = new Dataloader($uri);
		$this->assertInternalType('string', $dataLoader->getUri());
		$this->assertEquals($uri, $dataLoader->getUri());
		$newUri = 'http://www.sainsburys.co.uk/';
		$dataLoader->setUri($newUri);
		$this->assertNotEquals($uri, $dataLoader->getUri());
		$this->assertEquals($newUri, $dataLoader->getUri());

	}

	public function testQueryParameters()
	{
		$uri = 'http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html';
		$dataLoader = new Dataloader($uri);
		$this->assertInternalType('array', $dataLoader->getQueryParameters());
		$queryParameters = array("var1"=>"val1","var2"=>"val2");
		$dataLoader->setQueryParameters($queryParameters);
		$this->assertEquals($queryParameters, $dataLoader->getQueryParameters());
	}

	public function testData()
	{
		$uri = 'http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html';
		$dataLoader = new Dataloader($uri);
		$htmlData = $dataLoader->getData();
        $this->assertInstanceOf('\GuzzleHttp\Psr7\Stream', $htmlData);
        $this->assertGreaterThan(0, $htmlData->getSize() );
	}
}