<?php
namespace Victorfs\Sainsburys\Tests;

use Victorfs\Sainsburys\ProductCrawler;
use Victorfs\Sainsburys\Dataloader;
use Victorfs\Sainsburys\SainsburysApp;

class SainsburysAppTest extends \PHPUnit_Framework_TestCase
{
	private $_app;

	public function setUp()
	{
		$uri = 'http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html';
		$dataLoader = new Dataloader($uri);
		$productCrawler = new ProductCrawler();
		$this->_app = new SainsburysApp($dataLoader, $productCrawler);
	}

	public function testResponseArrayKeys()
	{
		$allProducts = $this->_app->getAllProdutcs();
		$this->assertInternalType('array', $allProducts);
		$this->assertArrayHasKey('results', $allProducts);
		$this->assertInternalType('array', $allProducts['results']);
		foreach ($allProducts['results'] as $product)
		{
			$this->assertArrayHasKey('title', $product);
			$this->assertArrayHasKey('size', $product);
			$this->assertArrayHasKey('unit_price', $product);
			$this->assertArrayHasKey('description', $product);
		}
		$this->assertArrayHasKey('total', $allProducts);
	}

	public function testResponseItems()
	{
		$allProducts = $this->_app->getAllProdutcs();
		foreach ($allProducts['results'] as $product)
		{
			$this->assertInternalType('string', $product['title']);
			$this->assertStringMatchesFormat('%skb', $product['size']);
			$this->assertGreaterThan(0, $product['unit_price']);
			$this->assertInternalType('string', $product['description']);
		}
        $this->assertGreaterThan(0, $allProducts['total'] );
	}

	public function testFirstProductResponse()
	{
		$allProducts = $this->_app->getAllProdutcs();
		$firstProduct = reset($allProducts['results']);
		$this->assertEquals('Sainsbury\'s Apricot Ripe & Ready x5', $firstProduct['title']);
		$this->assertEquals('38.3kb', $firstProduct['size']);
		$this->assertEquals(3.5, $firstProduct['unit_price']);
		$this->assertEquals('Apricots', $firstProduct['description']);
	}

	public function testNumberOfProducts()
	{
		$allProducts = $this->_app->getAllProdutcs();
		$this->assertCount(7, $allProducts['results']);
	}

	public function testTotalPrice()
	{
		$allProducts = $this->_app->getAllProdutcs();
		$this->assertEquals(15.1, $allProducts['total']);
	}

	public function testJsonResponse()
	{
		$allProducts = $this->_app->getAllProdutcs();
		$allProductsJSON = $this->_app->getAllProdutcsJSON();
		$this->assertJsonStringEqualsJsonString(json_encode($allProducts), $allProductsJSON);
	}

}